import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';
import React, {Suspense, lazy, Fragment} from 'react';
import SkewLoader from "react-spinners/SkewLoader";
import {
    ToastContainer,
} from 'react-toastify';

const Dashboards = lazy(() => import('../../DemoPages/Dashboards'));

const AppMain = () => {

    return (
        <Fragment>
            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                    <SkewLoader
                        size={30}
                        color={"#141e30"}/>
                    </div>
                </div>
            }>
                <Dashboards/>
            </Suspense>
            <ToastContainer/>
        </Fragment>
    )
};

export default AppMain;