import React, {Fragment} from 'react';

class AppFooter extends React.Component {
    render() {


        return (
            <Fragment>
                <div className="app-footer">
                    <div className="app-footer__inner">
                        <div className="app-footer-left">
                            <ul className="nav">
                                <li className="nav-item">
                                    <a href="https://www.linkedin.com/in/nahmedalizai/" target="_blank" className="nav-link">
                                       Developer
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a href="https://dashboardpack.com/theme-details/architectui-dashboard-react-pro/" target="_blank" className="nav-link">
                                        Design credits
                                    </a>
                                </li>
                                <li className="nav-item nav-link">
                                    beta 0.0.1
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </Fragment>
        )}
}

export default AppFooter;