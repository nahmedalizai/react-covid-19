import React, { useEffect } from 'react'
import { Row, Col, Card } from 'reactstrap';

import {
    faAngleUp,
    faArrowRight,
    faArrowUp,
    faArrowLeft,
    faAngleDown,
    faChartArea
} from '@fortawesome/free-solid-svg-icons';

const Countries = (props) => {

    const [countryList, setCountryList] = React.useState([]);
    useEffect(() => {
        setCountryList(props.list)
        return () => {
        }
    }, [countryList])

    function formatNumber(stringNumber) {
        if(stringNumber !== null && stringNumber !== "")
            return stringNumber.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')
        return ""
    }

    const tableRowItems = countryList.map(function(el, index) {
          return <tr key={index}>
                <td><div className="font-size-xlg">{el.country}</div></td>
                <td className="text-center font-size-xlg"><div className="badge badge-info">{
                    el.cases.total ? formatNumber(el.cases.total) : 0
                }</div></td>
                <td className="text-center font-size-xlg"><div className="badge badge-danger">{
                    el.cases.new ? formatNumber(el.cases.new) : 0
                }</div></td>
                <td className="text-center font-size-xlg"><div className="badge badge-warning">{
                    el.cases.active ? formatNumber(el.cases.active) : 0
                }</div></td>
                <td className="text-center font-size-xlg"><div className="badge badge-success">{
                    el.cases.recovered ? formatNumber(el.cases.recovered) : 0
                }</div></td>
                <td className="text-center font-size-xlg"><div className="badge badge-danger">{
                    el.deaths.total ? formatNumber(el.deaths.total) : 0
                }</div></td>
            </tr>
        });

    return (
        <Row>
            <Col md="12">
                <Card className="main-card mb-3">
                    <div className="card-header">Countries {window.innerWidth < 600 ? "<-scroll->" : ""}
                    {/* <div className="btn-actions-pane-right">
                            <div role="group" className="btn-group-sm btn-group">
                                <button className="active btn btn-info">Last Week</button>
                                <button className="btn btn-info">All Month</button>
                            </div>
                        </div> */}
                    </div>
                    <div className="table-responsive">
                        <table className="align-middle mb-0 table table-borderless table-striped table-hover">
                            <thead>
                                <tr>
                                    <th className="font-size-xlg">Name</th>
                                    <th className="text-center font-size-xlg">Total</th>
                                    <th className="text-center font-size-xlg">New</th>
                                    <th className="text-center font-size-xlg">Active</th>
                                    <th className="text-center font-size-xlg">Recovered</th>
                                    <th className="text-center font-size-xlg">Deaths</th>
                                </tr>
                            </thead>
                            <tbody>
                                {tableRowItems}
                            </tbody>
                        </table>
                    </div>
                    {/* <div className="d-block text-center card-footer">
                        <button className="mr-2 btn-icon btn-icon-only btn btn-outline-danger"><i className="pe-7s-trash btn-icon-wrapper"> </i></button>
                        <button className="btn-wide btn btn-success">Save</button>
                    </div> */}
                </Card>
            </Col>
        </Row>
    )
}

export default Countries;
