import axios from 'axios';

const GetStatistics = async (request, country, date) => {
    if (request !== undefined && request !== null && request !== "") {
        return (await axios({
            method: 'GET',
            url: 'https://covid-193.p.rapidapi.com/' + request,
            params: request === "history" ? { "day" : date, "country": country} : "",
            headers: {
                'Content-Type': 'application/octet-stream',
                "x-rapidapi-host": "covid-193.p.rapidapi.com",
                "x-rapidapi-key": process.env.REACT_APP_COVID_19_API_KEY
            }
        }))
    }
}

export default GetStatistics;