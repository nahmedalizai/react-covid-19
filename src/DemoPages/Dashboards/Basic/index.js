import React, { Component, Fragment, useEffect, useState } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import classnames from 'classnames';
import CountriesList from './CountriesList';
import GetStatistics from './GetStatistics';
import SkewLoader from "react-spinners/SkewLoader";
import {
    Row, Col,
    Button,
    CardHeader,
    Card,
    CardBody,
    Progress,
    TabContent,
    TabPane,
} from 'reactstrap';

import PageTitle from '../../../Layout/AppMain/PageTitle';

import {
    AreaChart, Area, Line,
    ResponsiveContainer,
    Bar,
    BarChart,
    ComposedChart,
    CartesianGrid,
    Tooltip,
    LineChart
} from 'recharts';

import {
    faAngleUp,
    faArrowRight,
    faArrowUp,
    faArrowLeft,
    faAngleDown,
    faChartArea
} from '@fortawesome/free-solid-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function AnalyticsDashboard1() {

    const [dropdownOpen, setDropdownOpen] = useState(false)
    const [activeTab1, setActiveTab1] = useState('11')
    const [statData, setStatData] = useState([])
    const [lastUpdate, setLastUpdate] = useState("")
    const [status, setStatus] = useState({})
    const [chartData, setChartData] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    function getFormatedDate() {
        var d = new Date(Date.now() - 5 * 24 * 60 * 60 * 1000),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    useEffect(() => {
        GetStatistics("statistics", null, null).then(res => {
            if (res != null && res.status === 200) {
                if (res && res.data && res.data.results > 0) {
                    setStatData(res.data.response)
                    var generalInfo = (res.data.response.filter(obj => {
                        return obj.country == "All"
                    }))[0]

                    setStatus(generalInfo)
                    setLastUpdate(generalInfo.time.substr(0, 10) + " " + generalInfo.time.substr(11, 8))

                    GetStatistics("history", "all", null).then(stats => {
                        if (stats != null && stats.status === 200) {
                            if (stats && stats.data && stats.data.results > 0) {
                                let items = [];
                                let i = 0, multiplier = 0;
                                if (stats.data.results > 10) {
                                    multiplier = Math.round(parseInt(stats.data.results) / 10);
                                }
                                while (i < stats.data.results) {
                                    items.push({
                                        total: parseInt(stats.data.response[i].cases.total),
                                        recovered: parseInt(stats.data.response[i].cases.recovered),
                                        death: parseInt(stats.data.response[i].deaths.total)
                                    })
                                    i = i + multiplier;
                                }

                                setChartData(items.reverse())

                            }
                        }
                        setIsLoading(false)
                    }).catch(error => {
                        console.log(error)
                        setIsLoading(false)
                    })
                }
            }
            setIsLoading(false)
        }).catch(error => {
            console.log(error)
            setIsLoading(false)
        });

        return () => {
        }
    }, [])

    function formatNumber(stringNumber) {
        if (stringNumber !== null && stringNumber !== "")
            return stringNumber.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')
        return ""
    }

    return (
        <Fragment>
            <ReactCSSTransitionGroup
                component="div"
                transitionName="TabsAnimation"
                transitionAppear={true}
                transitionAppearTimeout={0}
                transitionEnter={false}
                transitionLeave={false}>
                <div>
                    <PageTitle
                        heading="COVID-19 Information"
                        subheading={lastUpdate ? "Last updated at : " + lastUpdate : ""}
                        icon="pe-7s-settings icon-gradient bg-mean-fruit"
                    />
                    <div style={{ textAlign: "-webkit-center" }}>
                        <SkewLoader
                            color={"#141e30"}
                            size={20}
                            loading={isLoading} />
                    </div>
                    <Row>
                        <Col md="12" lg="6">
                            <Row>
                                <Col md="6">
                                    <div className="card mb-3 bg-arielle-smile widget-chart text-white card-border">
                                        <div className="icon-wrapper rounded-circle">
                                            <div className="icon-wrapper-bg bg-white opacity-8" />
                                            <i className="lnr-earth icon-gradient bg-arielle-smile" />
                                        </div>
                                        <div className="widget-numbers">
                                            Total
                                            </div>
                                        <div className="widget-numbers-sm">
                                            {status && status.cases ? formatNumber(status.cases.total) : "0"}
                                        </div>
                                        {/* <div className="widget-description text-white">
                                                <FontAwesomeIcon icon={faAngleUp}/>
                                                <span className="pl-1">54.9%</span>
                                            </div> */}
                                    </div>
                                </Col>
                                <Col md="6">
                                    <div className="card mb-3 bg-midnight-bloom widget-chart text-white card-border">
                                        <div className="icon-wrapper rounded">
                                            <div className="icon-wrapper-bg bg-white opacity-6" />
                                            <i className="lnr-leaf icon-gradient bg-warm-flame" />
                                        </div>
                                        <div className="widget-numbers">
                                            Active
                                            </div>
                                        <div className="widget-numbers-sm">
                                            {status && status.cases ? formatNumber(status.cases.active) : "0"}
                                        </div>
                                        {/* <div className="widget-description text-white">
                                                <span className="pr-1">62,7%</span>
                                                <FontAwesomeIcon icon={faArrowLeft}/>
                                            </div> */}
                                    </div>
                                </Col>
                                <Col md="6">
                                    <div className="card mb-3 bg-grow-early widget-chart text-white card-border">
                                        <div className="icon-wrapper rounded">
                                            <div className="icon-wrapper-bg bg-white opacity-6" />
                                            <i className="lnr-heart icon-gradient bg-warm-flame" />
                                        </div>
                                        <div className="widget-numbers">
                                            Recovered
                                            </div>
                                        <div className="widget-numbers-sm">
                                            {status && status.cases ? formatNumber(status.cases.recovered) : "0"}
                                        </div>
                                        {/* <div className="widget-description text-white">
                                                <FontAwesomeIcon icon={faArrowRight}/>
                                                <span className="pl-1">72.1%</span>
                                            </div> */}
                                    </div>
                                </Col>
                                <Col md="6">
                                    <div className="card mb-3 bg-love-kiss widget-chart card-border">
                                        <div className="widget-chart-content text-white">
                                            <div className="icon-wrapper rounded-circle">
                                                <div className="icon-wrapper-bg bg-white opacity-4" />
                                                <i className="lnr-hourglass" />
                                            </div>
                                            <div className="widget-numbers">
                                                Deaths
                                                </div>
                                            <div className="widget-numbers-sm">
                                                {status && status.deaths ? formatNumber(status.deaths.total) : "0"}
                                            </div>
                                            {/* <div className="widget-description">
                                                    <FontAwesomeIcon className="text-white opacity-5" icon={faAngleUp}/>
                                                    <span className="text-white">175.5%</span>
                                                </div> */}
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                        <Col md="12" lg="6">
                            <Card className="mb-3">
                                <CardHeader className="card-header-tab">
                                    <div className="card-header-title">
                                        <i className="header-icon lnr-chart-bars icon-gradient bg-tempting-azure"> </i>
                                            Overall Cases Report
                                        </div>
                                    {/* <div className="btn-actions-pane-right">
                                            <Button outline
                                                    className={"border-0 btn-pill btn-wide btn-transition " + classnames({active: activeTab1 === '11'})}
                                                    color="primary" onClick={() => {
                                                    toggle1('11');
                                            }}>Tab 1</Button>
                                            <Button outline
                                                    className={"ml-1 btn-pill btn-wide border-0 btn-transition " + classnames({active: activeTab1 === '22'})}
                                                    color="primary" onClick={() => {
                                                    toggle1('22');
                                            }}>Tab 2</Button>
                                        </div> */}
                                </CardHeader>
                                <TabContent activeTab={activeTab1}>
                                    <TabPane tabId="11">
                                        <div className="widget-chart p-0">
                                            <div className="widget-chart-content">
                                                <div className="widget-description mt-0 text-warning">
                                                    <FontAwesomeIcon icon={faChartArea} />
                                                    <span className="pl-1">Data</span>
                                                    <span className="text-muted opacity-8 pl-1">(Past 5 Days)</span>
                                                </div>
                                            </div>
                                            {/* <ResponsiveContainer height={187}>
                                                <AreaChart data={chartData} margin={{ top: -45, right: 0, left: 0, bottom: 0 }}>
                                                    <defs>
                                                        <linearGradient id="colorPv2" x1="0" y1="0" x2="0" y2="1">
                                                            <stop offset="10%" stopColor="var(--warning)" stopOpacity={0.7} />
                                                            <stop offset="90%" stopColor="var(--warning)" stopOpacity={0} />
                                                        </linearGradient>
                                                    </defs>
                                                    <Tooltip />
                                                    <Area type='monotoneX' dataKey='total' stroke='var(--warning)' strokeWidth={2} fillOpacity={1}
                                                        fill="url(#colorPv2)" />
                                                </AreaChart>
                                            </ResponsiveContainer> */}
                                            <ResponsiveContainer height={179}>
                                                <ComposedChart data={chartData ? chartData : []}>
                                                    <CartesianGrid stroke="#ffffff" />
                                                    <Area type="monotone" dataKey="total" fill="#f7ffd0" stroke="#85a200" />
                                                    <Bar dataKey="recovered" barSize={16} fill="var(--primary)" />
                                                    <Line type="monotone" dataKey="death" strokeWidth="3" stroke="var(--danger)" />
                                                    <Tooltip />
                                                </ComposedChart>
                                            </ResponsiveContainer>
                                            <div className="widget-chart-content mt-3 mb-2">
                                                <div className="widget-description mt-0 text-success">
                                                    <span className="pl-2 pr-2">total</span>
                                                    <span className="text-primary">recovered </span>
                                                    <span className="text-danger">deaths</span>
                                                </div>
                                            </div>
                                        </div>
                                        <CardBody className="pt-2">
                                            <Row className="mt-3">
                                                <Col md="6">
                                                    <div className="widget-content">
                                                        <div className="widget-content-outer">
                                                            <div className="widget-content-wrapper">
                                                                <div className="widget-content-left mr-3">
                                                                    <div className="widget-numbers fsize-3 text-muted">
                                                                        {
                                                                            status && status.cases ?
                                                                                Math.round(
                                                                                    (parseInt(status.cases.active) - parseInt(status.cases.critical)) * 100 / parseInt(status.cases.active)) + "%" : "0%"
                                                                        }                                                                        </div>
                                                                </div>
                                                                <div className="widget-content-right">
                                                                    <div className="text-muted opacity-8">
                                                                        Mild Condition
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <div className="widget-progress-wrapper mt-1">
                                                                <Progress
                                                                    className="progress-bar-sm progress-bar-animated-alt"
                                                                    color="primary"
                                                                    value={
                                                                        status && status.cases ?
                                                                            Math.round(
                                                                                (parseInt(status.cases.active) - parseInt(status.cases.critical)) * 100 / parseInt(status.cases.active)) : "0%"
                                                                    } />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <div className="divider mt-4" />
                                                <Col md="6">
                                                    <div className="widget-content">
                                                        <div className="widget-content-outer">
                                                            <div className="widget-content-wrapper">
                                                                <div className="widget-content-left mr-3">
                                                                    <div className="widget-numbers fsize-3 text-muted">
                                                                        {
                                                                            status && status.cases ?
                                                                                Math.round(parseInt(status.cases.critical) * 100 / parseInt(status.cases.active)) + "%" : "0%"
                                                                        }
                                                                    </div>
                                                                </div>
                                                                <div className="widget-content-right">
                                                                    <div className="text-muted opacity-8">
                                                                        Critial Condition
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <div className="widget-progress-wrapper mt-1">
                                                                <Progress
                                                                    className="progress-bar-sm progress-bar-animated-alt"
                                                                    color="warning"
                                                                    value={
                                                                        status && status.cases ?
                                                                            Math.round(parseInt(status.cases.critical) * 100 / parseInt(status.cases.active)) : "0%"
                                                                    } />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                            </Row>
                                            <div className="divider mt-4" />
                                            <Row>
                                                <Col md="6">
                                                    <div className="widget-content">
                                                        <div className="widget-content-outer">
                                                            <div className="widget-content-wrapper">
                                                                <div className="widget-content-left mr-3">
                                                                    <div className="widget-numbers fsize-3 text-muted">
                                                                        {
                                                                            status && status.cases ?
                                                                                Math.round(parseInt(status.cases.recovered) * 100 / parseInt(status.cases.total)) + "%" : "0"
                                                                        }
                                                                    </div>
                                                                </div>
                                                                <div className="widget-content-right">
                                                                    <div className="text-muted opacity-8">
                                                                        Recovered
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <div className="widget-progress-wrapper mt-1">
                                                                <Progress
                                                                    className="progress-bar-sm progress-bar-animated-alt"
                                                                    color="success"
                                                                    value={
                                                                        status && status.cases ?
                                                                            Math.round(parseInt(status.cases.recovered) * 100 / parseInt(status.cases.total)) : "0"
                                                                    } />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col md="6">
                                                    <div className="widget-content">
                                                        <div className="widget-content-outer">
                                                            <div className="widget-content-wrapper">
                                                                <div className="widget-content-left mr-3">
                                                                    <div className="widget-numbers fsize-3 text-muted">
                                                                        {
                                                                            status && status.cases && status.deaths ?
                                                                                Math.round(parseInt(status.deaths.total) * 100 / parseInt(status.cases.total)) + "%" : "0"
                                                                        }
                                                                    </div>
                                                                </div>
                                                                <div className="widget-content-right">
                                                                    <div className="text-muted opacity-8">
                                                                        Death
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <div className="widget-progress-wrapper mt-1">
                                                                <Progress
                                                                    className="progress-bar-sm progress-bar-animated-alt"
                                                                    color="danger"
                                                                    value={
                                                                        status && status.cases && status.deaths ?
                                                                            Math.round(parseInt(status.deaths.total) * 100 / parseInt(status.cases.total)) : "0"
                                                                    } />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </TabPane>
                                </TabContent>
                            </Card>
                        </Col>
                    </Row>
                    {statData.length > 0 && <CountriesList list={statData} />}
                </div>
            </ReactCSSTransitionGroup>
        </Fragment>
    )
}
